package com.tdenchyk.exception;

public class FailedActionException extends RuntimeException {
    public FailedActionException(String message) {
        super(message);
    }

    public FailedActionException(String message, Throwable cause) {
        super(message, cause);
    }
}
