package com.tdenchyk.dto.subject;

import lombok.Data;

@Data
public class DeleteSubjectDto {
    private String id;
}
