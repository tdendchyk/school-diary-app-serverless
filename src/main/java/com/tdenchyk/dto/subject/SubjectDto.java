package com.tdenchyk.dto.subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SubjectDto {
    private String id;
    private String subjectName;
    private String teacherId;
}
