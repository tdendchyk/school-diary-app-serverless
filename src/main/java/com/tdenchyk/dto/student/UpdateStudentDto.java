package com.tdenchyk.dto.student;

import lombok.Data;

@Data
public class UpdateStudentDto {

    private String email;

    private String gradeId;

    private String firstName;

    private String lastName;

    private String phoneNumber;
}
