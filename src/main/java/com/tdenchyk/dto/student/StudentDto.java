package com.tdenchyk.dto.student;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentDto {

    private String email;

    private String gradeId;

    private String firstName;

    private String lastName;

    private String phoneNumber;
}
