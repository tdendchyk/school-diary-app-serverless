package com.tdenchyk.dto.student;

import lombok.Data;

@Data
public class DeleteStudentDto {
    private String email;
    private String gradeId;
}
