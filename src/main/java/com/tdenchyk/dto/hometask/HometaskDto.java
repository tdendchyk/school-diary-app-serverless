package com.tdenchyk.dto.hometask;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HometaskDto {

    private String id;

    private String scheduleId;

    private String task;

    private String taskDeadline;
}
