package com.tdenchyk.dto.hometask;

import lombok.Data;

@Data
public class DeleteHometaskDto {

    private String id;

    private String scheduleId;
}
