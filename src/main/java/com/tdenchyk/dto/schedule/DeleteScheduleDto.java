package com.tdenchyk.dto.schedule;

import lombok.Data;

@Data
public class DeleteScheduleDto {

    private String id;

    private String gradeId;
}
