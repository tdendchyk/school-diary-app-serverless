package com.tdenchyk.dto.schedule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScheduleDto {

    private String id;

    private String gradeId;

    private String weekday;

    private String subjectId;

    private String startTime;

    private String endTime;
}
