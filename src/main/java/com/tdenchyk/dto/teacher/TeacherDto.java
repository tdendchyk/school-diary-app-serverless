package com.tdenchyk.dto.teacher;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TeacherDto {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String subjectId;
}
