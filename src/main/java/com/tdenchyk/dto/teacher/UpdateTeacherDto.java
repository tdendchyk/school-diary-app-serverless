package com.tdenchyk.dto.teacher;

import lombok.Data;

@Data
public class UpdateTeacherDto {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String subjectId;
}
