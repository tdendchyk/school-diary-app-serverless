package com.tdenchyk.dto.teacher;

import lombok.Data;

@Data
public class DeleteTeacherDto {
    private String email;
}
