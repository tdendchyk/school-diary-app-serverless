package com.tdenchyk.dto.grade;

import lombok.Data;

@Data
public class DeleteGradeDto {
    private String id;
}
