package com.tdenchyk.function.teacher;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.teacher.TeacherDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class CreateTeacherLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("TEACHERS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final AmazonSQS sqsClient = AmazonSQSClientBuilder.standard()
            .withRegion("eu-central-1")
            .build();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            TeacherDto teacherDto = objectMapper.readValue(requestBody, TeacherDto.class);

            log.info("Incoming input: " + teacherDto.toString());
            addNewTeacher(teacherDto);
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(201);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully added new teacher");
        return responseEvent;
    }

    private void addNewTeacher(TeacherDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item foundItem = table.getItem("email", input.getEmail());
        if (foundItem != null) {
            log.error("Failed to add new teacher with email: " + input.getEmail());
            throw new FailedActionException("Teacher with this email already exists.");
        }
        Item item = new Item()
                .withPrimaryKey("email", input.getEmail())
                .withString("firstName", input.getFirstName())
                .withString("lastName", input.getLastName())
                .withString("phoneNumber", input.getPhoneNumber())
                .withString("subjectId", input.getSubjectId());
        try {
            table.putItem(item);
        } catch (Exception ex) {
            throw new FailedActionException("Failed to add new teacher", ex);
        }
        log.info("Added new student and adding message to queue");
        String queueUrl = System.getenv("SQS_QUEUE_URL");
        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(input.getEmail());
        sqsClient.sendMessage(sendMessageRequest);
        log.info("Message sent to queue");
    }
}
