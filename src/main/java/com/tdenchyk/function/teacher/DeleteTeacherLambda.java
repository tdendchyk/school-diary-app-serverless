package com.tdenchyk.function.teacher;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.teacher.DeleteTeacherDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class DeleteTeacherLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("TEACHERS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            DeleteTeacherDto deleteTeacherDto = objectMapper.readValue(requestBody, DeleteTeacherDto.class);
            log.info("Request to delete teacher with email: " + deleteTeacherDto.getEmail());
            deleteTeacher(deleteTeacherDto.getEmail());
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(204);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully deleted teacher.");
        return responseEvent;
    }

    private void deleteTeacher(String email) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        try {
            DeleteItemSpec itemSpec = new DeleteItemSpec().withPrimaryKey("email", email);
            table.deleteItem(itemSpec);
        } catch (Exception ex) {
            log.error("Failed to delete teacher with email: " + email);
            throw new FailedActionException("Failed to delete teacher.");
        }
    }
}
