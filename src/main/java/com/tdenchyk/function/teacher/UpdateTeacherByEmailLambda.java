package com.tdenchyk.function.teacher;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.teacher.UpdateTeacherDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateTeacherByEmailLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("TEACHERS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        String requestBody = input.getBody();
        try {
            UpdateTeacherDto updateTeacherDto = objectMapper.readValue(requestBody, UpdateTeacherDto.class);
            log.info("Incoming input: " + updateTeacherDto.toString());
            return updateTeacherByEmail(updateTeacherDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateTeacherByEmail(UpdateTeacherDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item existingTeacher = table.getItem("email", input.getEmail());
        UpdateItemOutcome updateItemOutcome = null;
        if (existingTeacher != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("email", input.getEmail())
                    .withUpdateExpression("SET firstName = :fn, lastName = :ln, phoneNumber = :pn, subjectId = :sb")
                    .withValueMap(
                            new ValueMap().withString(":fn", input.getFirstName())
                                    .withString(":ln", input.getLastName())
                                    .withString(":pn", input.getPhoneNumber())
                                    .withString(":sb", input.getSubjectId())
                    )
                    .withReturnValues(ReturnValue.ALL_NEW);

            updateItemOutcome = table.updateItem(updateItemSpec);

            log.info("Teacher information updated successfully. Email: " + input.getEmail());
        } else {
            log.error("Teacher with email " + input.getEmail() + " does not exist.");
            throw new FailedActionException("Failed to update teacher with email: " + input.getEmail());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Teacher information updated successfully: "
                + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
