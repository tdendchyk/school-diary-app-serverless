package com.tdenchyk.function.hometask;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.hometask.HometaskDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateHometaskLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("HOMETASKS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        String requestBody = input.getBody();
        log.info("Request body: " + requestBody);
        try {
            HometaskDto hometaskDto = objectMapper.readValue(requestBody, HometaskDto.class);
            log.info("Incoming input: " + hometaskDto.toString());
            return updateHometask(hometaskDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateHometask(HometaskDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item foundItem = table
                .getItem("id", input.getId(), "scheduleId", input.getScheduleId());
        UpdateItemOutcome updateItemOutcome = null;
        if (foundItem != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", input.getId(),
                            "scheduleId", input.getScheduleId())
                    .withUpdateExpression("SET task = :t, taskDeadline = :dl")
                    .withValueMap(
                            new ValueMap().withString(":t", input.getTask())
                                    .withString(":dl", input.getTaskDeadline())
                    )
                    .withReturnValues(ReturnValue.ALL_NEW);
            updateItemOutcome = table.updateItem(updateItemSpec);
            log.info("Hometask information updated successfully. Id: " + input.getId());
        } else {
            log.error("Hometask with id " + input.getId() + " does not exist.");
            throw new FailedActionException("Failed to update hometask with id: " + input.getId());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Hometask information updated successfully: "
                + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
