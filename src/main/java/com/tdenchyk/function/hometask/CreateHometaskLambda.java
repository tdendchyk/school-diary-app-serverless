package com.tdenchyk.function.hometask;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.hometask.HometaskDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class CreateHometaskLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("HOMETASKS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            HometaskDto hometaskDto = objectMapper.readValue(requestBody, HometaskDto.class);

            log.info("Incoming input: " + hometaskDto.toString());
            addNewHometask(hometaskDto);
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(201);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully added new hometask");
        return responseEvent;
    }

    private void addNewHometask(HometaskDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item item = new Item()
                .withPrimaryKey("id", input.getId(),
                        "scheduleId", input.getScheduleId())
                .withString("task", input.getTask())
                .withString("taskDeadline", input.getTaskDeadline());
        try {
            table.putItem(item);
        } catch (Exception ex) {
            throw new FailedActionException("Failed to add new hometask", ex);
        }
    }
}
