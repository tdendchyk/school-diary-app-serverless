package com.tdenchyk.function.grade;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.grade.GradeDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateGradeLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("GRADES_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            GradeDto gradeDto = objectMapper.readValue(requestBody, GradeDto.class);
            log.info("Incoming input: " + gradeDto.toString());
            return updateGradeNumber(gradeDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateGradeNumber(GradeDto gradeDto) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item itemId = table.getItem("id", gradeDto.getId());
        UpdateItemOutcome updateItemOutcome = null;
        if (itemId != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", gradeDto.getId())
                    .withUpdateExpression("SET gradeNumber = :n")
                    .withValueMap(
                            new ValueMap().withInt(":n", gradeDto.getGradeNumber()))
                    .withReturnValues(ReturnValue.ALL_NEW);
            updateItemOutcome = table.updateItem(updateItemSpec);
            log.info("Grade number updated successfully. Id: " + gradeDto.getId());
        } else {
            log.error("Grade with id " + gradeDto.getId() + " does not exist.");
            throw new FailedActionException("Failed to update grade with id: " + gradeDto.getId());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully updated grade: " + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
