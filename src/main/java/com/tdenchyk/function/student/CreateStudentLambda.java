package com.tdenchyk.function.student;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.student.StudentDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class CreateStudentLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("STUDENTS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final AmazonSQS sqsClient = AmazonSQSClientBuilder.standard()
            .withRegion("eu-central-1")
            .build();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            StudentDto studentDto = objectMapper.readValue(requestBody, StudentDto.class);

            log.info("Incoming input: " + studentDto.toString());
            addNewStudent(studentDto);
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(201);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully added new student");
        return responseEvent;
    }

    private void addNewStudent(StudentDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item foundItem = table.getItem("email", input.getEmail(),
                "gradeId", input.getGradeId());
        log.info("Email: " + input.getEmail() + " GradeId: " + input.getGradeId());
        if (foundItem != null) {
            log.error("Failed to add new student with email: " + input.getEmail());
            throw new FailedActionException("Student with this email already exists.");
        }
        Item item = new Item()
                .withPrimaryKey("email", input.getEmail(),
                        "gradeId", input.getGradeId())
                .withString("firstName", input.getFirstName())
                .withString("lastName", input.getLastName())
                .withString("phoneNumber", input.getPhoneNumber());
        try {
            table.putItem(item);
        } catch (Exception ex) {
            throw new FailedActionException("Failed to add new student", ex);
        }
        log.info("Added new student and adding message to queue");
        String queueUrl = System.getenv("SQS_QUEUE_URL");
        SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(input.getEmail());
        sqsClient.sendMessage(sendMessageRequest);
        log.info("Message sent to queue");
    }
}
