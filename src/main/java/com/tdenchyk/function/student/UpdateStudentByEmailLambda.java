package com.tdenchyk.function.student;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.student.UpdateStudentDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateStudentByEmailLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("STUDENTS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        String requestBody = input.getBody();
        log.info("Request body: " + requestBody);
        try {
            UpdateStudentDto updateStudentDto = objectMapper.readValue(requestBody, UpdateStudentDto.class);
            log.info("Incoming input: " + updateStudentDto.toString());
            return updateStudentByEmail(updateStudentDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateStudentByEmail(UpdateStudentDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item existingStudent = table
                .getItem("email", input.getEmail(), "gradeId", input.getGradeId());
        UpdateItemOutcome updateItemOutcome = null;
        if (existingStudent != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("email", input.getEmail(),
                            "gradeId", input.getGradeId())
                    .withUpdateExpression("SET firstName = :fn, lastName = :ln, phoneNumber = :pn")
                    .withValueMap(
                            new ValueMap().withString(":fn", input.getFirstName())
                                    .withString(":ln", input.getLastName())
                                    .withString(":pn", input.getPhoneNumber())
                    )
                    .withReturnValues(ReturnValue.ALL_NEW);
            updateItemOutcome = table.updateItem(updateItemSpec);
            log.info("Student information updated successfully. Email: " + input.getEmail());
        } else {
            log.error("Student with email " + input.getEmail() + " does not exist.");
            throw new FailedActionException("Failed to update student with email: " + input.getEmail());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Student information updated successfully: "
                + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
