package com.tdenchyk.function.schedule;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.schedule.DeleteScheduleDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class DeleteScheduleLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("SCHEDULES_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            DeleteScheduleDto deleteScheduleDto = objectMapper.readValue(requestBody, DeleteScheduleDto.class);
            log.info("Request to delete schedule with id: " + deleteScheduleDto.getId());
            deleteSchedule(deleteScheduleDto.getId(), deleteScheduleDto.getGradeId());
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(204);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully deleted schedule.");
        return responseEvent;
    }

    private void deleteSchedule(String id, String gradeId) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        try {
            DeleteItemSpec itemSpec = new DeleteItemSpec()
                    .withPrimaryKey("id", id, "gradeId", gradeId);
            table.deleteItem(itemSpec);
        } catch (Exception ex) {
            log.error("Failed to delete schedule with id: " + id + ", and gradeId: " + gradeId);
            throw new FailedActionException("Failed to delete schedule.");
        }
    }
}
