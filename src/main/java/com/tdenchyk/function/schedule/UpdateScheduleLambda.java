package com.tdenchyk.function.schedule;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.schedule.ScheduleDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateScheduleLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("SCHEDULES_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        String requestBody = input.getBody();
        log.info("Request body: " + requestBody);
        try {
            ScheduleDto scheduleDto = objectMapper.readValue(requestBody, ScheduleDto.class);
            log.info("Incoming input: " + scheduleDto.toString());
            return updateSchedule(scheduleDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateSchedule(ScheduleDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item foundItem = table
                .getItem("id", input.getId(), "gradeId", input.getGradeId());
        UpdateItemOutcome updateItemOutcome = null;
        if (foundItem != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", input.getId(),
                            "gradeId", input.getGradeId())
                    .withUpdateExpression("SET weekday = :wd, subjectId = :s, startTime = :st, endTime = :et")
                    .withValueMap(
                            new ValueMap().withString(":wd", input.getWeekday())
                                    .withString(":s", input.getSubjectId())
                                    .withString(":st", input.getStartTime())
                                    .withString(":et", input.getEndTime())
                    )
                    .withReturnValues(ReturnValue.ALL_NEW);
            updateItemOutcome = table.updateItem(updateItemSpec);
            log.info("Schedule information updated successfully. Id: " + input.getId());
        } else {
            log.error("Schedule with id " + input.getId() + " does not exist.");
            throw new FailedActionException("Failed to update schedule with id: " + input.getId());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Schedule information updated successfully: "
                + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
