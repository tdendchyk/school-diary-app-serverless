package com.tdenchyk.function.schedule;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.schedule.ScheduleDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class CreateScheduleLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("SCHEDULES_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        try {
            String requestBody = input.getBody();
            ScheduleDto scheduleDto = objectMapper.readValue(requestBody, ScheduleDto.class);

            log.info("Incoming input: " + scheduleDto.toString());
            addNewSchedule(scheduleDto);
        } catch (IOException ex) {
            log.error("Error parsing request body", ex);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(201);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Successfully added new schedule");
        return responseEvent;
    }

    private void addNewSchedule(ScheduleDto input) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item item = new Item()
                .withPrimaryKey("id", input.getId(),
                        "gradeId", input.getGradeId())
                .withString("weekday", input.getWeekday())
                .withString("subjectId", input.getSubjectId())
                .withString("startTime", input.getStartTime())
                .withString("endTime", input.getEndTime());
        try {
            table.putItem(item);
        } catch (Exception ex) {
            throw new FailedActionException("Failed to add new schedule", ex);
        }
    }
}
