package com.tdenchyk.function;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SendEmailLambda implements RequestHandler<SQSEvent, Void> {
    private final AmazonSimpleEmailService sesClient = AmazonSimpleEmailServiceClientBuilder.defaultClient();

    @Override
    public Void handleRequest(SQSEvent event, Context context) {
        for (SQSEvent.SQSMessage msg : event.getRecords()) {
            String emailAddress = msg.getBody();
            log.info("Sending email to: " + emailAddress);

            SendEmailRequest emailRequest = new SendEmailRequest()
                    .withDestination(new Destination().withToAddresses(emailAddress))
                    .withMessage(new Message()
                            .withBody(new Body().withText(
                                    new Content()
                                            .withCharset("UTF-8")
                                            .withData("You are successfully registered!")))
                            .withSubject(new Content().withCharset("UTF-8").withData("Welcome to Your Diary!")))
                    .withSource("tetiana.mailing.list@gmail.com");
            sesClient.sendEmail(emailRequest);
            log.info("Email sent to: " + emailAddress);
        }
        return null;
    }
}