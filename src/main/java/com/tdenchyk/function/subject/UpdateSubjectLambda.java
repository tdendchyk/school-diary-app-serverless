package com.tdenchyk.function.subject;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdenchyk.dto.subject.SubjectDto;
import com.tdenchyk.exception.FailedActionException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Collections;

@Log4j2
public class UpdateSubjectLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final String TABLE_NAME = System.getenv("SUBJECTS_TABLE");
    private final DynamoDB dynamoDB = new DynamoDB(AmazonDynamoDBClientBuilder.defaultClient());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        String requestBody = input.getBody();
        try {
            SubjectDto subjectDto = objectMapper.readValue(requestBody, SubjectDto.class);
            log.info("Incoming input: " + subjectDto.toString());
            return updateSubject(subjectDto);
        } catch (IOException ex) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(400)
                    .withBody("Error parsing request body");
        }
    }

    private APIGatewayProxyResponseEvent updateSubject(SubjectDto subjectDto) {
        Table table = dynamoDB.getTable(TABLE_NAME);
        Item subject = table.getItem("id", subjectDto.getId());
        UpdateItemOutcome updateItemOutcome = null;
        if (subject != null) {
            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", subjectDto.getId())
                    .withUpdateExpression("SET subjectName = :n, teacherId = :t")
                    .withValueMap(
                            new ValueMap().withString(":n", subjectDto.getSubjectName())
                                    .withString(":t", subjectDto.getTeacherId()))
                    .withReturnValues(ReturnValue.ALL_NEW);
            updateItemOutcome = table.updateItem(updateItemSpec);
            log.info("Subject information updated successfully. Id: " + subjectDto.getId());
        } else {
            log.error("Subject with id " + subjectDto.getId() + " does not exist.");
            throw new FailedActionException("Failed to update subject with id: " + subjectDto.getId());
        }
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        responseEvent.setStatusCode(200);
        responseEvent.setHeaders(Collections.singletonMap("Content-Type", "application/json"));
        responseEvent.setBody("Subject information updated successfully: "
                + updateItemOutcome.getUpdateItemResult().toString());
        return responseEvent;
    }
}
