##  Your Diary*📓 Online School Diary*
### Serverless Application
<hr>



### Introduction

*Welcome to Your Diary!* 👋
<br>

This app is created to assist both teachers and students.

### This app was created via *Serverless Framework* and following *Amazon Web Services* were used:
* **Lambda**
* **API Gateway**
* **DynamoDB**
* **CloudWatch**
* **CloudFormation**

### Other technologies used: 
* **Java 11**
* **Lombok**
* **Log4j2**

